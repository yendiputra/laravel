<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashbordController;
use App\Http\Controllers\IndexController;


Route::get('/', 'IndexController@home');
Route::get('/registrasi', 'AuthController@registrasi');
Route::get('/welcome', 'AuthController@welcome');
